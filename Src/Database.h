/*

   ____ ___                      _                   
 / ___|_ _|_ ____   _____ _ __ | |_ ___  _ __ _   _ 
| |    | || '_ \ \ / / _ \ '_ \| __/ _ \| '__| | | |
| |___ | || | | \ V /  __/ | | | || (_) | |  | |_| |
 \____|___|_| |_|\_/ \___|_| |_|\__\___/|_|   \__, |
                                              |___/ 

A C Program For Keeping Track Of An Inventory.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


#ifndef CINVENTORY_DATABASE_H
#define CINVENTORY_DATABASE_H


#include "Prereqs.h"
#include "Defs.h"



typedef _Call_Back_ LONG (*LPFN_INVENTORY_QUERY_PROC)(
	_In_ 		LPVOID 			lpParam,
	_In_ 		LONG 			lRowCount,
	_In_ 		DLPSTR 			dlpszValues,
	_In_ 		DLPSTR 			dlpszColumns
);



_Success_(return != NULL_OBJECT, _Non_Locking_)
CINVENTORY_API
HANDLE
OpenInventoryDatabase(
	_In_ 		LPCSTR RESTRICT 	lpcszFile
);




_Success_(return != FALSE, _Interlocked_Operation_)
CINVENTORY_API
BOOL 
RunInventoryQuery(
	_In_ 		HANDLE 				hInventory,
	_In_ 		LPCSTR 				lpcszQuery,
	_In_ 		LPFN_INVENTORY_QUERY_PROC	lpfnCallBack,
	_In_ 		LPVOID 				lpParam
);




_Success_(return != FALSE, _Interlocked_Operation_)
CINVENTORY_API
BOOL 
InventoryAddNewItem(
	_In_ 		HANDLE 				hInventory,
	_In_ 		LPCSTR 				lpcszItem
);




_Success_(return != FALSE, _Interlocked_Operation_)
CINVENTORY_API
BOOL 
InventoryAddNewInventory(
	_In_ 		HANDLE 				hInventory,
	_In_ 		ULONG 				ulItemID,
	_In_ 		ULONG 				ulQuantity,
	_In_ 		LPCSTR 				lpcszAcquireDate,
	_In_ 		LPCSTR 				lpcszExpirationDate,
	_In_ 		FLOAT 				fCostPerUnit,
	_In_ 		FLOAT 				fTotalCost,
	_In_Z_ 		LPCSTR 				lpcszNotes
);




_Success_(return != FALSE, _Interlocked_Operation_)
CINVENTORY_API
BOOL 
InventoryEditItem(
	_In_ 		HANDLE 				hInventory,
	_In_ 		ULONG 				ulID,
	_In_ 		LPCSTR 				lpcszItem
);




_Success_(return != FALSE, _Interlocked_Operation_)
CINVENTORY_API
BOOL 
InventoryEditInventory(
	_In_ 		HANDLE 				hInventory,
	_In_ 		ULONG 				ulID,
	_In_ 		ULONG 				ulItemID,
	_In_ 		ULONG 				ulQuantity,
	_In_ 		LPCSTR 				lpcszAcquireDate,
	_In_ 		LPCSTR 				lpcszExpirationDate,
	_In_ 		FLOAT 				fCostPerUnit,
	_In_ 		FLOAT 				fTotalCost,
	_In_Z_ 		LPCSTR 				lpcszNotes
);




_Success_(return != NULLPTR, _Interlocked_Operation_)
CINVENTORY_API
HSTRING 
InventoryItemIdToString(
	_In_ 		HANDLE 				hInventory,
	_In_ 		ULONG 				ulID
);




_Success_(return != FALSE, _Interlocked_Operation_)
CINVENTORY_API
BOOL
InventoryListItems(
	_In_ 		HANDLE 				hInventory
);




_Success_(return != FALSE, _Interlocked_Operation_)
CINVENTORY_API
BOOL
InventoryListItemsToString(
	_In_ 		HANDLE 				hInventory,
	_Out_Z_ 	LPSTR 				lpszOut,
	_In_		UARCHLONG 			ualBufferSize
);




_Success_(return != FALSE, _Interlocked_Operation_)
CINVENTORY_API
BOOL
InventoryListInventory(
	_In_ 		HANDLE 				hInventory
);




_Success_(return != FALSE, _Interlocked_Operation_)
CINVENTORY_API
BOOL
InventoryListInventoryById(
	_In_ 		HANDLE 				hInventory,
	_In_ 		ULONG 				ulItemID
);




_Success_(return != FALSE, _Interlocked_Operation_)
CINVENTORY_API
ULONG
InventoryGetQuantityById(
	_In_ 		HANDLE 				hInventory,
	_In_ 		ULONG 				ulItemID
);




_Success_(return != FALSE, _Interlocked_Operation_)
CINVENTORY_API
ULONG
InventorySetQuantityById(
	_In_ 		HANDLE 				hInventory,
	_In_ 		ULONG 				ulItemID,
	_In_ 		ULONG 				ulQuantity
);




_Success_(return != FALSE, _Interlocked_Operation_)
CINVENTORY_API
BOOL
InventoryIncrementQuantityById(
	_In_ 		HANDLE 				hInventory,
	_In_ 		ULONG 				ulItemID,
	_In_ 		ULONG 				ulDelta
);




_Success_(return != FALSE, _Interlocked_Operation_)
CINVENTORY_API
BOOL
InventoryDecrementQuantityById(
	_In_ 		HANDLE 				hInventory,
	_In_ 		ULONG 				ulItemID,
	_In_ 		ULONG 				ulDelta
);




_Success_(return != FALSE, _Interlocked_Operation_)
CINVENTORY_API 
BOOL 
InventorySumTotalCost(
	_In_ 		HANDLE 				hInventory
);




_Success_(return != FALSE, _Interlocked_Operation_)
CINVENTORY_API 
BOOL 
InventorySumQuantity(
	_In_ 		HANDLE 				hInventory
);



#endif