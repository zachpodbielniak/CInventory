/*

   ____ ___                      _                   
 / ___|_ _|_ ____   _____ _ __ | |_ ___  _ __ _   _ 
| |    | || '_ \ \ / / _ \ '_ \| __/ _ \| '__| | | |
| |___ | || | | \ V /  __/ | | | || (_) | |  | |_| |
 \____|___|_| |_|\_/ \___|_| |_|\__\___/|_|   \__, |
                                              |___/ 

A C Program For Keeping Track Of An Inventory.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


#include "Database.h"
#include "DatabaseObject.c"



typedef struct __DB_STRING_PARAM
{
	LPSTR 		lpszData;
	UARCHLONG	ualSize;
	BOOL 		bHasPrintedColumns;
} DB_STRING_PARAM, *LPDB_STRING_PARAM;




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL 
__DestroyInventory(
	_In_ 		HDERIVATIVE 		hDerivative,
	_In_Opt_ 	ULONG 			ulFlags
)
{	UNREFERENCED_PARAMETER(ulFlags);
	EXIT_IF_UNLIKELY_NULL(hDerivative, FALSE);

	LPINVENTORY_DATABASE lpInvDb;
	lpInvDb = (LPINVENTORY_DATABASE)hDerivative;

	WaitForSingleObject(lpInvDb->hLock, INFINITE_WAIT_TIME);

	sqlite3_close(lpInvDb->slDb);
	DestroyObject(lpInvDb->hLock);
	FreeMemory(lpInvDb);

	return TRUE;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
CINVENTORY_API
HANDLE
OpenInventoryDatabase(
	_In_ 		LPCSTR RESTRICT 	lpcszFile
){
	EXIT_IF_UNLIKELY_NULL(lpcszFile, NULL_OBJECT);

	HANDLE hInventory;
	LPINVENTORY_DATABASE lpInvDb;

	lpInvDb = GlobalAllocAndZero(sizeof(INVENTORY_DATABASE));
	if (NULLPTR == lpInvDb)
	{ return NULL_OBJECT; }

	lpInvDb->hLock = CreateMutex();
	if (NULL_OBJECT == lpInvDb->hLock)
	{ 
		FreeMemory(lpInvDb);
		return NULL_OBJECT;
	}

	if (0 != sqlite3_open(lpcszFile, &(lpInvDb->slDb)))
	{
		DestroyObject(lpInvDb->hLock);
		FreeMemory(lpInvDb);
		return NULL_OBJECT;
	}

	hInventory = CreateHandleWithSingleInheritor(
		lpInvDb,
		&(lpInvDb->hThis),
		HANDLE_TYPE_INVENTORY,
		__DestroyInventory,
		0,
		NULLPTR,
		0,
		NULLPTR,
		0,
		&(lpInvDb->ullId),
		0
	);

	if (NULL_OBJECT == hInventory)
	{
		sqlite3_close(lpInvDb->slDb);
		DestroyObject(lpInvDb->hLock);
		FreeMemory(lpInvDb);
		return NULL_OBJECT;
	}

	return hInventory;
}




_Success_(return != FALSE, _Interlocked_Operation_)
CINVENTORY_API
BOOL 
RunInventoryQuery(
	_In_ 		HANDLE 				hInventory,
	_In_ 		LPCSTR 				lpcszQuery,
	_In_ 		LPFN_INVENTORY_QUERY_PROC	lpfnCallBack,
	_In_ 		LPVOID 				lpParam
){
	EXIT_IF_UNLIKELY_NULL(hInventory, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpcszQuery, FALSE);

	LPINVENTORY_DATABASE lpInvDb;
	lpInvDb = OBJECT_CAST(hInventory, LPINVENTORY_DATABASE);

	EXIT_IF_UNLIKELY_NULL(lpInvDb, FALSE);

	WaitForSingleObject(lpInvDb->hLock, INFINITE_WAIT_TIME);

	sqlite3_exec(
		lpInvDb->slDb,
		lpcszQuery,
		lpfnCallBack,
		lpParam,
		&(lpInvDb->lpszError)
	);
	
	if (NULLPTR != lpInvDb->lpszError)
	{
		PrintFormat("%s\n", lpInvDb->lpszError);
		lpInvDb->lpszError = NULLPTR;
		return FALSE;
	}

	ReleaseSingleObject(lpInvDb->hLock);
	return TRUE;
}




_Success_(return != FALSE, _Interlocked_Operation_)
CINVENTORY_API
BOOL 
InventoryAddNewItem(
	_In_ 		HANDLE 				hInventory,
	_In_ 		LPCSTR 				lpcszItem
){
	EXIT_IF_UNLIKELY_NULL(hInventory, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpcszItem, FALSE);

	LPINVENTORY_DATABASE lpInvDb;
	CSTRING csQuery[8192];

	lpInvDb = OBJECT_CAST(hInventory, LPINVENTORY_DATABASE);
	EXIT_IF_UNLIKELY_NULL(lpInvDb, FALSE);

	ZeroMemory(csQuery, sizeof(csQuery));

	StringPrintFormatSafe(
		csQuery,
		sizeof(csQuery) - 1,
		"INSERT INTO Items(\"Name\") VALUES(\"%s\");",
		lpcszItem
	);

	return RunInventoryQuery(
		hInventory,
		(LPCSTR)csQuery,
		NULLPTR,
		NULLPTR
	);
}




_Success_(return != FALSE, _Interlocked_Operation_)
CINVENTORY_API
BOOL 
InventoryAddNewInventory(
	_In_ 		HANDLE 				hInventory,
	_In_ 		ULONG 				ulItemID,
	_In_ 		ULONG 				ulQuantity,
	_In_ 		LPCSTR 				lpcszAcquireDate,
	_In_ 		LPCSTR 				lpcszExpirationDate,
	_In_ 		FLOAT 				fCostPerUnit,
	_In_ 		FLOAT 				fTotalCost,
	_In_Z_ 		LPCSTR 				lpcszNotes
){
	EXIT_IF_UNLIKELY_NULL(hInventory, FALSE);
	EXIT_IF_UNLIKELY_NULL(ulItemID, FALSE);
	EXIT_IF_UNLIKELY_NULL(ulQuantity, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpcszAcquireDate, FALSE);


	LPINVENTORY_DATABASE lpInvDb;
	CSTRING csQuery[8192];

	lpInvDb = OBJECT_CAST(hInventory, LPINVENTORY_DATABASE);
	EXIT_IF_UNLIKELY_NULL(lpInvDb, FALSE);

	ZeroMemory(csQuery, sizeof(csQuery));

	StringPrintFormatSafe(
		csQuery,
		sizeof(csQuery) - 1,
		"INSERT INTO Inventory("
			"\"ItemID\", \"Quantity\", \"AcquiredDate\","
			"\"ExpirationDate\", \"CostPerUnit\", \"TotalCost\","
			"\"Notes\""
		") VALUES("
			"%u, %u, date(\"%s\"), date(\"%s\"), %f, %f, \"%s\""
		");",
		ulItemID,
		ulQuantity,
		lpcszAcquireDate,
		lpcszExpirationDate,
		fCostPerUnit,
		fTotalCost,
		lpcszNotes
	);

	return RunInventoryQuery(
		hInventory,
		(LPCSTR)csQuery,
		NULLPTR,
		NULLPTR
	);
}




_Success_(return != FALSE, _Interlocked_Operation_)
CINVENTORY_API
BOOL 
InventoryEditItem(
	_In_ 		HANDLE 				hInventory,
	_In_ 		ULONG 				ulID,
	_In_ 		LPCSTR 				lpcszItem
){
	EXIT_IF_UNLIKELY_NULL(hInventory, FALSE);
	EXIT_IF_UNLIKELY_NULL(ulID, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpcszItem, FALSE);

	LPINVENTORY_DATABASE lpInvDb;
	CSTRING csQuery[8192];

	lpInvDb = OBJECT_CAST(hInventory, LPINVENTORY_DATABASE);
	EXIT_IF_UNLIKELY_NULL(lpInvDb, FALSE);

	ZeroMemory(csQuery, sizeof(csQuery));

	StringPrintFormatSafe(
		csQuery,
		sizeof(csQuery) - 1,
		"UPDATE Items SET Name = \"%s\" WHERE ID = %u;",
		lpcszItem,
		ulID
	);

	return RunInventoryQuery(
		hInventory,
		(LPCSTR)csQuery,
		NULLPTR,
		NULLPTR
	);
}




_Success_(return != FALSE, _Interlocked_Operation_)
CINVENTORY_API
BOOL 
InventoryEditInventory(
	_In_ 		HANDLE 				hInventory,
	_In_ 		ULONG 				ulID,
	_In_ 		ULONG 				ulItemID,
	_In_ 		ULONG 				ulQuantity,
	_In_ 		LPCSTR 				lpcszAcquireDate,
	_In_ 		LPCSTR 				lpcszExpirationDate,
	_In_ 		FLOAT 				fCostPerUnit,
	_In_ 		FLOAT 				fTotalCost,
	_In_Z_ 		LPCSTR 				lpcszNotes
){
	EXIT_IF_UNLIKELY_NULL(hInventory, FALSE);
	EXIT_IF_UNLIKELY_NULL(ulItemID, FALSE);
	EXIT_IF_UNLIKELY_NULL(ulQuantity, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpcszAcquireDate, FALSE);


	LPINVENTORY_DATABASE lpInvDb;
	CSTRING csQuery[8192];

	lpInvDb = OBJECT_CAST(hInventory, LPINVENTORY_DATABASE);
	EXIT_IF_UNLIKELY_NULL(lpInvDb, FALSE);

	ZeroMemory(csQuery, sizeof(csQuery));

	StringPrintFormatSafe(
		csQuery,
		sizeof(csQuery) - 1,
		"UPDATE Inventory SET "
			"ItemID = %u,"
			"Quantity = %u,"
			"AcquiredDate = \"%s\","
			"ExpirationDate = \"%s\","
			"CostPerUnit = %f,"
			"TotalCost = %f,"
			"Notes = \"%s\" "
		"WHERE ID = %u",
		ulItemID,
		ulQuantity,
		lpcszAcquireDate,
		lpcszExpirationDate,
		fCostPerUnit,
		fTotalCost,
		lpcszNotes,
		ulID
	);

	return RunInventoryQuery(
		hInventory,
		(LPCSTR)csQuery,
		NULLPTR,
		NULLPTR
	);
}




_Success_(return == 0, _Non_Locking_)
INTERNAL_OPERATION
LONG
__InventoryItemIdToString(
	_In_ 		LPVOID 			lpParam,
	_In_ 		LONG 			lRowCount,
	_In_ 		DLPSTR 			dlpszValues,
	_In_ 		DLPSTR 			dlpszColumns
){
	UNREFERENCED_PARAMETER(dlpszColumns);
	EXIT_IF_UNLIKELY_NULL(lpParam, 1);
	EXIT_IF_UNLIKELY_NULL(dlpszValues, 1);

	HSTRING *lphsResult;
	lphsResult = lpParam;

	if (1 != lRowCount)
	{ return 1; }

	*lphsResult = CreateHeapString(dlpszValues[0]);
	return 0;
}




_Success_(return != NULLPTR, _Interlocked_Operation_)
CINVENTORY_API
HSTRING 
InventoryItemIdToString(
	_In_ 		HANDLE 				hInventory,
	_In_ 		ULONG 				ulID
){
	EXIT_IF_UNLIKELY_NULL(hInventory, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(ulID, FALSE);

	LPINVENTORY_DATABASE lpInvDb;
	HSTRING hsResult;
	CSTRING csQuery[8192];

	lpInvDb = OBJECT_CAST(hInventory, LPINVENTORY_DATABASE);
	EXIT_IF_UNLIKELY_NULL(lpInvDb, FALSE);

	ZeroMemory(csQuery, sizeof(csQuery));

	StringPrintFormatSafe(
		csQuery,
		sizeof(csQuery) - 1,
		"SELECT Name FROM Items WHERE ID = %u;",
		ulID
	);

	RunInventoryQuery(
		hInventory,
		(LPCSTR)csQuery,
		__InventoryItemIdToString,
		&hsResult
	);

	return hsResult;
}




_Success_(return == 0, _Non_Locking_)
INTERNAL_OPERATION
LONG
__InventoryListItems(
	_In_ 		LPVOID 			lpParam,
	_In_ 		LONG 			lRowCount,
	_In_ 		DLPSTR 			dlpszValues,
	_In_ 		DLPSTR 			dlpszColumns
){
	UNREFERENCED_VARIABLE(lRowCount);
	LPBOOL lpbHasPrintedColumns;
	UARCHLONG ualIndex;

	lpbHasPrintedColumns = lpParam;

	if (FALSE == *lpbHasPrintedColumns)
	{
		UARCHLONG ualSize;
		CSTRING csColumn[512];
		ZeroMemory(csColumn, sizeof(csColumn));

		StringPrintFormatSafe(
			csColumn,
			sizeof(csColumn) - 1,
			"%s\t%s",
			dlpszColumns[0],
			dlpszColumns[1]
		);

		PrintFormat("%s\n", csColumn);
		ualSize = StringLength(csColumn);
		ZeroMemory(csColumn, sizeof(csColumn));

		for (
			ualIndex = 0;
			ualIndex < ualSize + 5;
			ualIndex++
		){
			StringConcatenateSafe(csColumn, "-", sizeof(csColumn) - 1);
		}
		PrintFormat("%s\n", csColumn);

		*lpbHasPrintedColumns = TRUE;
	}

	PrintFormat("%s\t%s\n", dlpszValues[0], dlpszValues[1]);

	return 0;
}




_Success_(return != FALSE, _Interlocked_Operation_)
CINVENTORY_API
BOOL
InventoryListItems(
	_In_ 		HANDLE 				hInventory
){
	LPINVENTORY_DATABASE lpInvDb;
	CSTRING csQuery[8192];
	BOOL bHasPrintedColumns;

	lpInvDb = OBJECT_CAST(hInventory, LPINVENTORY_DATABASE);
	EXIT_IF_UNLIKELY_NULL(lpInvDb, FALSE);

	ZeroMemory(csQuery, sizeof(csQuery));
	bHasPrintedColumns = FALSE;

	StringPrintFormatSafe(
		csQuery,
		sizeof(csQuery) - 1,
		"SELECT * FROM Items;"
	);

	RunInventoryQuery(
		hInventory,
		(LPCSTR)csQuery,
		__InventoryListItems,
		&bHasPrintedColumns
	);

	return TRUE;
}




_Success_(return == 0, _Non_Locking_)
INTERNAL_OPERATION
LONG
__InventoryListItemsToString(
	_In_ 		LPVOID 			lpParam,
	_In_ 		LONG 			lRowCount,
	_In_ 		DLPSTR 			dlpszValues,
	_In_ 		DLPSTR 			dlpszColumns
){
	UNREFERENCED_VARIABLE(lRowCount);
	UARCHLONG ualIndex;
	LPDB_STRING_PARAM lpdspData;

	lpdspData = lpParam;


	if (FALSE == lpdspData->bHasPrintedColumns)
	{
		UARCHLONG ualSize;
		CSTRING csColumn[512];
		ZeroMemory(csColumn, sizeof(csColumn));

		StringPrintFormatSafe(
			csColumn,
			sizeof(csColumn) - 1,
			"%s\t%s\n",
			dlpszColumns[0],
			dlpszColumns[1]
		);
		
		StringConcatenateSafe(lpdspData->lpszData, csColumn, lpdspData->ualSize - 1);
		ualSize = StringLength(csColumn);
		ZeroMemory(csColumn, sizeof(csColumn));

		for (
			ualIndex = 0;
			ualIndex < ualSize + 5;
			ualIndex++
		){
			StringConcatenateSafe(csColumn, "-", sizeof(csColumn) - 1);
		}
		StringConcatenateSafe(lpdspData->lpszData, csColumn, lpdspData->ualSize - 1);
		StringConcatenateSafe(lpdspData->lpszData, "\n", lpdspData->ualSize - 1);

		lpdspData->bHasPrintedColumns = TRUE;
	}

	StringConcatenateSafe(lpdspData->lpszData, dlpszValues[0], lpdspData->ualSize - 1);
	StringConcatenateSafe(lpdspData->lpszData, "\t", lpdspData->ualSize - 1);
	StringConcatenateSafe(lpdspData->lpszData, dlpszValues[1], lpdspData->ualSize - 1);
	StringConcatenateSafe(lpdspData->lpszData, "\n", lpdspData->ualSize - 1);

	return 0;
}




_Success_(return != FALSE, _Interlocked_Operation_)
CINVENTORY_API
BOOL
InventoryListItemsToString(
	_In_ 		HANDLE 				hInventory,
	_Out_Z_ 	LPSTR 				lpszOut,
	_In_		UARCHLONG 			ualBufferSize
){
	LPINVENTORY_DATABASE lpInvDb;
	CSTRING csQuery[1024];
	DB_STRING_PARAM dspData;

	lpInvDb = OBJECT_CAST(hInventory, LPINVENTORY_DATABASE);
	EXIT_IF_UNLIKELY_NULL(lpInvDb, FALSE);

	ZeroMemory(csQuery, sizeof(csQuery));
	dspData.bHasPrintedColumns = FALSE;
	dspData.lpszData = lpszOut;
	dspData.ualSize = ualBufferSize;

	StringPrintFormatSafe(
		csQuery,
		sizeof(csQuery) - 1,
		"SELECT * FROM Items;"
	);

	RunInventoryQuery(
		hInventory,
		(LPCSTR)csQuery,
		__InventoryListItemsToString,
		&dspData
	);

	return TRUE;
}




_Success_(return == 0, _Non_Locking_)
INTERNAL_OPERATION
LONG
__InventoryListInventory(
	_In_ 		LPVOID 			lpParam,
	_In_ 		LONG 			lRowCount,
	_In_ 		DLPSTR 			dlpszValues,
	_In_ 		DLPSTR 			dlpszColumns
){
	UNREFERENCED_VARIABLE(lRowCount);
	LPBOOL lpbHasPrintedColumns;
	UARCHLONG ualIndex;
	FLOAT fCostPerUnit, fTotalCost;

	lpbHasPrintedColumns = lpParam;

	if (FALSE == *lpbHasPrintedColumns)
	{
		UARCHLONG ualSize;
		CSTRING csColumn[512];

		ZeroMemory(csColumn, sizeof(csColumn));

		StringPrintFormatSafe(
			csColumn,
			sizeof(csColumn) - 1,
			"%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s",
			dlpszColumns[0],
			dlpszColumns[1],
			dlpszColumns[2],
			dlpszColumns[3],
			dlpszColumns[4],
			dlpszColumns[5],
			dlpszColumns[6],
			dlpszColumns[7]
		);

		PrintFormat("%s\n", csColumn);
		ualSize = StringLength(csColumn);
		ZeroMemory(csColumn, sizeof(csColumn));

		for (
			ualIndex = 0;
			ualIndex < ualSize + 35;
			ualIndex++
		){
			StringConcatenateSafe(csColumn, "-", sizeof(csColumn) - 1);
		}
		PrintFormat("%s\n", csColumn);

		*lpbHasPrintedColumns = TRUE;
	}

	StringScanFormat(dlpszValues[5], "%f", &fCostPerUnit);
	StringScanFormat(dlpszValues[6], "%f", &fTotalCost);

	PrintFormat(
		"%s\t%s\t%s\t\t%s\t%s\t$%.3f\t\t$%.2f\t\t%s\n", 
		dlpszValues[0], dlpszValues[1],
		dlpszValues[2], dlpszValues[3],
		dlpszValues[4], fCostPerUnit,
		fTotalCost, dlpszValues[7]
	);

	return 0;
}




_Success_(return != FALSE, _Interlocked_Operation_)
CINVENTORY_API
BOOL
InventoryListInventory(
	_In_ 		HANDLE 				hInventory
){
	LPINVENTORY_DATABASE lpInvDb;
	CSTRING csQuery[8192];
	BOOL bHasPrintedColumns;

	lpInvDb = OBJECT_CAST(hInventory, LPINVENTORY_DATABASE);
	EXIT_IF_UNLIKELY_NULL(lpInvDb, FALSE);

	ZeroMemory(csQuery, sizeof(csQuery));
	bHasPrintedColumns = FALSE;

	StringPrintFormatSafe(
		csQuery,
		sizeof(csQuery) - 1,
		"SELECT * FROM Inventory;"
	);

	RunInventoryQuery(
		hInventory,
		(LPCSTR)csQuery,
		__InventoryListInventory,
		&bHasPrintedColumns
	);

	return TRUE;
}




_Success_(return == 0, _Non_Locking_)
INTERNAL_OPERATION
LONG
__InventoryListInventoryById(
	_In_ 		LPVOID 			lpParam,
	_In_ 		LONG 			lRowCount,
	_In_ 		DLPSTR 			dlpszValues,
	_In_ 		DLPSTR 			dlpszColumns
){
	UNREFERENCED_VARIABLE(lRowCount);
	UARCHLONG ualIndex;
	LPBOOL lpbHasPrintedColumn;
	
	lpbHasPrintedColumn = lpParam;

	if (FALSE == *lpbHasPrintedColumn)
	{
		UARCHLONG ualSize;
		CSTRING csColumn[512];
		ZeroMemory(csColumn, sizeof(csColumn));

		StringPrintFormatSafe(
			csColumn,
			sizeof(csColumn) - 1,
			"%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s",
			dlpszColumns[0],
			dlpszColumns[1],
			dlpszColumns[2],
			dlpszColumns[3],
			dlpszColumns[4],
			dlpszColumns[5],
			dlpszColumns[6],
			dlpszColumns[7]
		);

		PrintFormat("%s\n", csColumn);
		ualSize = StringLength(csColumn);
		ZeroMemory(csColumn, sizeof(csColumn));

		for (
			ualIndex = 0;
			ualIndex < ualSize + 35;
			ualIndex++
		){
			StringConcatenateSafe(csColumn, "-", sizeof(csColumn) - 1);
		}
		PrintFormat("%s\n", csColumn);

		*lpbHasPrintedColumn = TRUE;
	}

	PrintFormat(
		"%s\t%s\t%s\t\t%s\t%s\t%s\t\t%s\t\t%s\n", 
		dlpszValues[0], dlpszValues[1],
		dlpszValues[2], dlpszValues[3],
		dlpszValues[4], dlpszValues[5],
		dlpszValues[6], dlpszValues[7]
	);

	return 0;
}




_Success_(return != FALSE, _Interlocked_Operation_)
CINVENTORY_API
BOOL
InventoryListInventoryById(
	_In_ 		HANDLE 				hInventory,
	_In_ 		ULONG 				ulItemID
){
	EXIT_IF_UNLIKELY_NULL(hInventory, FALSE);
	EXIT_IF_UNLIKELY_NULL(ulItemID, FALSE);

	LPINVENTORY_DATABASE lpInvDb;
	CSTRING csQuery[8192];
	BOOL bHasPrintedColumns;

	lpInvDb = OBJECT_CAST(hInventory, LPINVENTORY_DATABASE);
	EXIT_IF_UNLIKELY_NULL(lpInvDb, FALSE);

	ZeroMemory(csQuery, sizeof(csQuery));
	bHasPrintedColumns = FALSE;

	StringPrintFormatSafe(
		csQuery,
		sizeof(csQuery) - 1,
		"SELECT * FROM Inventory WHERE ItemID = %u",
		ulItemID
	);

	RunInventoryQuery(
		hInventory,
		(LPCSTR)csQuery,
		__InventoryListInventoryById,
		&bHasPrintedColumns
	);

	return TRUE;
}




_Success_(return == 0, _Non_Locking_)
INTERNAL_OPERATION
LONG
__InventoryGetQuantityById(
	_In_ 		LPVOID 			lpParam,
	_In_ 		LONG 			lRowCount,
	_In_ 		DLPSTR 			dlpszValues,
	_In_ 		DLPSTR 			dlpszColumns
){
	EXIT_IF_UNLIKELY_NULL(lpParam, 1);
	UNREFERENCED_PARAMETER(lRowCount);
	UNREFERENCED_PARAMETER(dlpszColumns);

	LPULONG lpulCount;
	lpulCount = lpParam;

	StringScanFormat(dlpszValues[0], "%u", lpulCount);
	return 0;
}




_Success_(return != FALSE, _Interlocked_Operation_)
CINVENTORY_API
ULONG
InventoryGetQuantityById(
	_In_ 		HANDLE 				hInventory,
	_In_ 		ULONG 				ulItemID
){
	EXIT_IF_UNLIKELY_NULL(hInventory, FALSE);
	EXIT_IF_UNLIKELY_NULL(ulItemID, FALSE);

	LPINVENTORY_DATABASE lpInvDb;
	CSTRING csQuery[8192];
	ULONG ulOut;

	lpInvDb = OBJECT_CAST(hInventory, LPINVENTORY_DATABASE);
	EXIT_IF_UNLIKELY_NULL(lpInvDb, FALSE);

	ZeroMemory(csQuery, sizeof(csQuery));

	StringPrintFormatSafe(
		csQuery,
		sizeof(csQuery) - 1,
		"SELECT Quantity FROM Inventory WHERE ID = %u",
		ulItemID
	);

	RunInventoryQuery(
		hInventory,
		(LPCSTR)csQuery,
		__InventoryGetQuantityById,
		&ulOut
	);

	return ulOut;
}




_Success_(return != FALSE, _Interlocked_Operation_)
CINVENTORY_API
ULONG
InventorySetQuantityById(
	_In_ 		HANDLE 				hInventory,
	_In_ 		ULONG 				ulItemID,
	_In_ 		ULONG 				ulQuantity
){
	EXIT_IF_UNLIKELY_NULL(hInventory, FALSE);
	EXIT_IF_UNLIKELY_NULL(ulItemID, FALSE);

	LPINVENTORY_DATABASE lpInvDb;
	CSTRING csQuery[8192];

	lpInvDb = OBJECT_CAST(hInventory, LPINVENTORY_DATABASE);
	EXIT_IF_UNLIKELY_NULL(lpInvDb, FALSE);

	ZeroMemory(csQuery, sizeof(csQuery));

	StringPrintFormatSafe(
		csQuery,
		sizeof(csQuery) - 1,
		"UPDATE Inventory SET Quantity = %u WHERE ID = %u",
		ulQuantity,
		ulItemID
	);

	RunInventoryQuery(
		hInventory,
		(LPCSTR)csQuery,
		NULLPTR,
		NULLPTR
	);

	return ulQuantity;
}




_Success_(return != FALSE, _Interlocked_Operation_)
CINVENTORY_API
BOOL
InventoryIncrementQuantityById(
	_In_ 		HANDLE 				hInventory,
	_In_ 		ULONG 				ulItemID,
	_In_ 		ULONG 				ulDelta
){
	EXIT_IF_UNLIKELY_NULL(hInventory, FALSE);
	EXIT_IF_UNLIKELY_NULL(ulItemID, FALSE);

	ULONGLONG ullSanity;
	ULONG ulCurrent;

	ulCurrent = InventoryGetQuantityById(hInventory, ulItemID);
	ullSanity = ulCurrent + ulDelta;

	if ((ULONGLONG)MAX_ULONG >= ullSanity)
	{ InventorySetQuantityById(hInventory, ulItemID, ulCurrent + ulDelta); }
	else 
	{ return FALSE; }

	return TRUE;
}




_Success_(return != FALSE, _Interlocked_Operation_)
CINVENTORY_API
BOOL
InventoryDecrementQuantityById(
	_In_ 		HANDLE 				hInventory,
	_In_ 		ULONG 				ulItemID,
	_In_ 		ULONG 				ulDelta
){
	EXIT_IF_UNLIKELY_NULL(hInventory, FALSE);
	EXIT_IF_UNLIKELY_NULL(ulItemID, FALSE);

	ULONGLONG ullSanity;
	ULONG ulCurrent;

	ulCurrent = InventoryGetQuantityById(hInventory, ulItemID);
	ullSanity = (ULONGLONG)ulCurrent - ulDelta;

	if (ullSanity < (ULONGLONG)ulCurrent)
	{ InventorySetQuantityById(hInventory, ulItemID, ulCurrent - ulDelta); }
	else 
	{ return FALSE; }

	return TRUE;
}




_Success_(return == 0, _Non_Locking_)
INTERNAL_OPERATION
LONG
__InventorySumToFloat(
	_In_ 		LPVOID 			lpParam,
	_In_ 		LONG 			lRowCount,
	_In_ 		DLPSTR 			dlpszValues,
	_In_ 		DLPSTR 			dlpszColumns
){
	UNREFERENCED_VARIABLE(lRowCount);
	UNREFERENCED_PARAMETER(dlpszColumns);
	
	LPFLOAT lpfOut;
	lpfOut = (LPFLOAT)lpParam;

	StringScanFormat(dlpszValues[0], "%f", lpfOut);
	return 0;
}




_Success_(return != FALSE, _Interlocked_Operation_)
CINVENTORY_API 
BOOL 
InventorySumTotalCost(
	_In_ 		HANDLE 				hInventory
){
	EXIT_IF_UNLIKELY_NULL(hInventory, FALSE);

	LPINVENTORY_DATABASE lpInvDb;
	FLOAT fSum;
	CSTRING csQuery[8192];

	lpInvDb = OBJECT_CAST(hInventory, LPINVENTORY_DATABASE);
	EXIT_IF_UNLIKELY_NULL(lpInvDb, FALSE);

	ZeroMemory(csQuery, sizeof(csQuery));

	StringPrintFormatSafe(
		csQuery,
		sizeof(csQuery) - 1,
		"SELECT SUM(TotalCost) FROM Inventory;"
	);

	RunInventoryQuery(
		hInventory,
		(LPCSTR)csQuery,
		__InventorySumToFloat,
		&fSum
	);

	PrintFormat("$%.2f\n", fSum);
	return TRUE;
}




_Success_(return != FALSE, _Interlocked_Operation_)
CINVENTORY_API 
BOOL 
InventorySumQuantity(
	_In_ 		HANDLE 				hInventory
){
	EXIT_IF_UNLIKELY_NULL(hInventory, FALSE);

	LPINVENTORY_DATABASE lpInvDb;
	FLOAT fSum;
	CSTRING csQuery[8192];

	lpInvDb = OBJECT_CAST(hInventory, LPINVENTORY_DATABASE);
	EXIT_IF_UNLIKELY_NULL(lpInvDb, FALSE);

	ZeroMemory(csQuery, sizeof(csQuery));

	StringPrintFormatSafe(
		csQuery,
		sizeof(csQuery) - 1,
		"SELECT SUM(Quantity) FROM Inventory;"
	);

	RunInventoryQuery(
		hInventory,
		(LPCSTR)csQuery,
		__InventorySumToFloat,
		&fSum
	);

	PrintFormat("%.0f\n", fSum);
	return TRUE;
}