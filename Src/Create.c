/*

   ____ ___                      _                   
 / ___|_ _|_ ____   _____ _ __ | |_ ___  _ __ _   _ 
| |    | || '_ \ \ / / _ \ '_ \| __/ _ \| '__| | | |
| |___ | || | | \ V /  __/ | | | || (_) | |  | |_| |
 \____|___|_| |_|\_/ \___|_| |_|\__\___/|_|   \__, |
                                              |___/ 

A C Program For Keeping Track Of An Inventory.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


#include "Create.h"




_Success_(return != FALSE, _Non_Locking_)
CINVENTORY_API
BOOL 
CreateInventoryDatabase(
	_In_ 		LPCSTR RESTRICT 	lpcszFile
){
	EXIT_IF_UNLIKELY_NULL(lpcszFile, FALSE);

	CreateInventoryItemTable(lpcszFile);
	CreateInventoryInventoryTable(lpcszFile);

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
CINVENTORY_API
BOOL 
CreateInventoryItemTable(
	_In_ 		LPCSTR RESTRICT 	lpcszFile
){
	sqlite3 *db;
	LPSTR lpszError;
	LPSTR lpszStatement;

	db = NULLPTR;
	lpszError = NULLPTR;

	lpszStatement = 	"DROP TABLE IF EXISTS Items;"
				"CREATE TABLE Items("
					"ID INTEGER PRIMARY KEY AUTOINCREMENT,"
					"Name VARCHAR(255) NOT NULL"
				");";

	sqlite3_open(lpcszFile, &db);
	if (NULLPTR == db)
	{ return FALSE; }

	sqlite3_exec(db, lpszStatement, NULLPTR, NULLPTR, &lpszError);
	if (NULLPTR != lpszError)
	{ 
		PrintFormat("ERROR: %s\n", lpszError);
		sqlite3_close(db);
		return FALSE;
	}

	sqlite3_close(db);
	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
CINVENTORY_API
BOOL 
CreateInventoryInventoryTable(
	_In_ 		LPCSTR RESTRICT 	lpcszFile
){
	sqlite3 *db;
	LPSTR lpszError;
	LPSTR lpszStatement;

	db = NULLPTR;
	lpszError = NULLPTR;

	lpszStatement = 	"DROP TABLE IF EXISTS Inventory;"
				"CREATE TABLE Inventory("
					"ID INTEGER PRIMARY KEY AUTOINCREMENT,"
					"ItemID INT NOT NULL,"
					"Quantity INT NOT NULL,"
					"AcquiredDate DATE NOT NULL,"
					"ExpirationDate DATE NOT NULL,"
					"CostPerUnit FLOAT NOT NULL,"
					"TotalCost FLOAT NOT NULL,"
					"Notes VARCHAR(511),"
					"FOREIGN KEY(ItemID) REFERENCES Items(ID)"
				");";

	sqlite3_open(lpcszFile, &db);
	if (NULLPTR == db)
	{ return FALSE; }

	sqlite3_exec(db, lpszStatement, NULLPTR, NULLPTR, &lpszError);
	if (NULLPTR != lpszError)
	{ 
		PrintFormat("ERROR: %s\n", lpszError);
		sqlite3_close(db);
		return FALSE;
	}

	sqlite3_close(db);
	return TRUE;
}