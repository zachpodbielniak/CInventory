/*

   ____ ___                      _                   
 / ___|_ _|_ ____   _____ _ __ | |_ ___  _ __ _   _ 
| |    | || '_ \ \ / / _ \ '_ \| __/ _ \| '__| | | |
| |___ | || | | \ V /  __/ | | | || (_) | |  | |_| |
 \____|___|_| |_|\_/ \___|_| |_|\__\___/|_|   \__, |
                                              |___/ 

A C Program For Keeping Track Of An Inventory.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


#include <PodNet/CContainers/CContainers.h>
#include <PodNet/CProcess/CProcess.h>
#include <CRock/CRock.h>
#include <CRock/Post.h>
#include "Create.h"
#include "Database.h"


typedef struct __STATE 
{
	LONG 		lArgCount;
	DLPSTR 		dlpszArgValues;

	HANDLE 		hHttpServer;
	HVECTOR 	hvBindAddresses;
	HVECTOR 	hvBindPorts;
} STATE, *LPSTATE;


INTERNAL_OPERATION
VOID 
__ParseArgs(
	_In_ 		LPSTATE 	lpstProgram
){
	UARCHLONG ualIndex;
	LPSTR lpszKey, lpszValue;

	if (1 == lpstProgram->lArgCount)
	{ return; }

	for (
		ualIndex = 1;
		ualIndex < (UARCHLONG)lpstProgram->lArgCount;
		ualIndex++
	){
		lpszKey = lpstProgram->dlpszArgValues[ualIndex];

		if (ualIndex <= (UARCHLONG)(lpstProgram->lArgCount - 1))
		{ lpszValue = lpstProgram->dlpszArgValues[ualIndex + 1]; }
		else 
		{ lpszValue = NULLPTR; }

		if (
			0 == StringCompare("-h", lpszKey) ||
			0 == StringCompare("--help", lpszKey)
		){}

		else if (
			0 == StringCompare("-b", lpszKey) ||
			0 == StringCompare("--bind", lpszKey)
		){
			if (NULLPTR == lpszValue)
			{ PostQuitMessage(1); }

			VectorPushBack(lpstProgram->hvBindAddresses, &lpszValue, 0);
		}

		else if (
			0 == StringCompare("-p", lpszKey) ||
			0 == StringCompare("--port", lpszKey)
		){
			USHORT usPort;

			if (NULLPTR == lpszValue)
			{ PostQuitMessage(1); }

			StringScanFormat(
				lpszValue,
				"%hu",
				&usPort
			);

			VectorPushBack(lpstProgram->hvBindPorts, &usPort, 0);
		}
	}
}



INTERNAL_OPERATION
VOID 
__NoQueryString(
	_Out_ 		LPSTR 			lpszOutput,
	_In_ 		UARCHLONG		ualOutputSize
){
	StringPrintFormatSafe(
		lpszOutput,
		ualOutputSize,
		"<h2>Inventory Web Access</h2>\r\n"
		"<div>To utilize Inventory Web Access do the following</div>"
	);
}



_Call_Back_
HTTP_STATUS_CODE
__RootHandler(
	_In_ 		HANDLE			hHttpServer,
	_In_Z_ 		LPCSTR RESTRICT 	lpcszUri,
	_In_ 		HTTP_METHOD 		hmType,
	_In_Opt_ 	LPVOID 			lpUserData,
	_In_ 		LPREQUEST_DATA		lprdData
){
	UNREFERENCED_PARAMETER(hHttpServer);
	UNREFERENCED_PARAMETER(lpcszUri);
	UNREFERENCED_PARAMETER(hmType);
	UNREFERENCED_PARAMETER(lpUserData);

	LPQUERYSTRING_DATA lpqsdData;
	UARCHLONG ualIndex;
	CSTRING csBody[8192];
	CSTRING csTemp[8192];

	LPSTR lpszFile;
	LPSTR lpszOp;

	lpszFile = NULLPTR;


	ZeroMemory(csBody, sizeof(csBody));
	ZeroMemory(csTemp, sizeof(csTemp));
	

	ProcessQueryStringParameters(lprdData->hRequest);

	if (0 == VectorSize(lprdData->hvQueryParameters))
	{ __NoQueryString((LPSTR)csBody, sizeof(csBody) - 1); }
	else 
	{
		/* Expect To look for 'File' */
		for (
			ualIndex = 0;
			ualIndex < VectorSize(lprdData->hvQueryParameters);
			ualIndex++
		){
			lpqsdData = VectorAt(lprdData->hvQueryParameters, ualIndex);

			if (
				0 == StringCompare("file", lpqsdData->lpszKey) ||
				0 == StringCompare("File", lpqsdData->lpszKey) || 
				0 == StringCompare("FILE", lpqsdData->lpszKey)
			){ lpszFile = lpqsdData->lpszValue; }
			else if (
				0 == StringCompare("operation", lpqsdData->lpszKey) ||
				0 == StringCompare("Operation", lpqsdData->lpszKey) || 
				0 == StringCompare("OPERATION", lpqsdData->lpszKey)
			){ lpszOp = lpqsdData->lpszValue; }
		}

		if (NULLPTR == lpszFile)
		{ __NoQueryString(csBody, sizeof(csBody) - 1); }
		else
		{
			HANDLE hInventory;
			hInventory = OpenInventoryDatabase(lpszFile);

			if (0 == StringCompare(lpszOp, "list-items"))
			{

				if (NULL_OBJECT == hInventory)
				{ return HTTP_STATUS_500_INTERNAL_SERVER_ERROR; }

				InventoryListItemsToString(hInventory, (LPSTR)csTemp, sizeof(csTemp) - 1);
				StringConcatenateSafe(csBody, csTemp, sizeof(csBody) - 1);		

			}
			
			DestroyObject(hInventory);
		}
	}
	

	RequestAddHeader(lprdData->hRequest, "Content-Type: text/plain;charset=utf-8");
	RequestAddResponse(lprdData->hRequest, csBody, StringLength(csBody));

	return HTTP_STATUS_200_OK;
}




LONG 
Main(
	_In_ 		LONG 		lArgCount,
	_In_Z_ 		DLPSTR 		dlpszArgValues
){
	STATE stProgram;
	ZeroMemory(&stProgram, sizeof(STATE));


	stProgram.lArgCount = lArgCount;
	stProgram.dlpszArgValues = dlpszArgValues;
	stProgram.hvBindAddresses = CreateVector(0x04U, sizeof(LPSTR), 0);
	stProgram.hvBindPorts = CreateVector(0x04U, sizeof(SHORT), 0);

	__ParseArgs(&stProgram);


	stProgram.hHttpServer = CreateHttpServerEx(
		"InventoryWebAccess",
		(DLPCSTR)VectorAt(stProgram.hvBindAddresses, 0),
		VectorSize(stProgram.hvBindAddresses),
		(LPUSHORT)VectorAt(stProgram.hvBindPorts, 0),
		VectorSize(stProgram.hvBindPorts),
		"Inventory",
		65536,
		4,
		0
	);

	if (NULL_OBJECT == stProgram.hHttpServer)
	{ PostQuitMessage(2); }

	DefineHttpUriProc(
		stProgram.hHttpServer,
		"/",
		HTTP_METHOD_GET,
		__RootHandler,
		&stProgram
	);

	LongSleep(INFINITE_WAIT_TIME);

	return 0;
}