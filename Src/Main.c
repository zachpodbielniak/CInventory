/*

   ____ ___                      _                   
 / ___|_ _|_ ____   _____ _ __ | |_ ___  _ __ _   _ 
| |    | || '_ \ \ / / _ \ '_ \| __/ _ \| '__| | | |
| |___ | || | | \ V /  __/ | | | || (_) | |  | |_| |
 \____|___|_| |_|\_/ \___|_| |_|\__\___/|_|   \__, |
                                              |___/ 

A C Program For Keeping Track Of An Inventory.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


#include "Create.h"
#include "Database.h"


typedef enum __COMAND
{
	COMMAND_CREATE_DATABASE			= 1 << 0,
	COMMAND_INSERT_DATABASE			= 1 << 1,
	COMMAND_QUERY_DATABASE			= 1 << 2,
	COMMAND_INSERT_ITEM			= 1 << 3,
	COMMAND_INSERT_INVENTORY		= 1 << 4,
	COMMAND_LIST_ITEM			= 1 << 5,
	COMMAND_LIST_INVENTORY			= 1 << 6,
	COMMAND_LIST_INVENTORY_BY_ID		= 1 << 7,
	COMMAND_EDIT_ITEM			= 1 << 8,
	COMMAND_EDIT_INVENTORY			= 1 << 9,
	COMMAND_RUN_QUERY			= 1 << 10,
	COMMAND_INCREMENT_INVENTORY		= 1 << 11,
	COMMAND_DECREMENT_INVENTORY		= 1 << 12,
	COMMAND_PRINT_SUM_TOTALCOST		= 1 << 13,
	COMMAND_PRINT_SUM_QUANTITY		= 1 << 14
} COMMAND;



typedef struct __STATE
{
	HANDLE 		hInventory;
	COMMAND 	cmdOperation;

	LPSTR 		lpszFile;
	LPSTR 		lpszItem;
	LPSTR 		lpszEditItem;
	LPSTR 		lpszInventory;
	LPSTR 		lpszEditInventory;
	LPSTR 		lpszInventoryByID;
	LPSTR 		lpszQuery;
	LPSTR 		lpszIncrement;
	LPSTR 		lpszDecrement;

	LONG 		lArgCount;
	DLPSTR 		dlpszArgValues;
} STATE, *LPSTATE;


_Kills_Process_
INTERNAL_OPERATION
KILL 
__PrintHelp(
	VOID
){
	PrintFormat("A C Program For Keeping Track Of An Inventory.\n");
	PrintFormat("Copyright (C) 2019 Zach Podbielniak\n");
	PrintFormat("----------------------------------------------\n");
	PrintFormat("\t-h --help\t\t\tShow this menu.\n");
	PrintFormat("\t-l --license\t\t\tShow license info.\n");
	PrintFormat("\t-f --file\t\t\tSpecify the database file.\n");
	PrintFormat("\t-c --create\t\t\tCreate a new database. Requires -f option.\n");
	PrintFormat("\t-a --add-item\t\t\tAdd a new item to the Items database.\n");
	PrintFormat("\t-A --add-inventory\t\tAdd new inventory listing for a corresponding item.\n");
	PrintFormat("\t-l --list-items\t\t\tList items in the database.\n");
	PrintFormat("\t-L --list-inventory\t\tList the entire inventory.\n");
	PrintFormat("\t-I --list-inventory-by-id\tList inventory by ItemID.\n");
	PrintFormat("\t-e --edit-item\t\t\tEdit the corresponding item with the provided ID.\n");
	PrintFormat("\t-E --edit-inventory\t\tEdit the corresponding inventory with the ID.\n");
	PrintFormat("\t-i --increment\t\t\tIncrement inventory quantity.\n");
	PrintFormat("\t-d --decrement\t\t\tDecrement inventory by quantity.\n");
	PrintFormat("\t-s --sum-cost\t\t\tPrint the sum of the TotalCost.\n");
	PrintFormat("\t-S --sum-quantity\t\tPrint the sume of the Quantity.\n");
	PrintFormat("\t-r --run-query\t\t\tRun a raw SQL query against the Database.\n");
	PostQuitMessage(0);
}




_Kills_Process_
INTERNAL_OPERATION
KILL 
__PrintLicense(
	VOID
){
	PrintFormat("A C Program For Keeping Track Of An Inventory.\n");
	PrintFormat("Copyright (C) 2019 Zach Podbielniak\n");
	PrintFormat("\n");
	PrintFormat("This program is free software: you can redistribute it and/or modify\n");
	PrintFormat("it under the terms of the GNU Affero General Public License as\n");
	PrintFormat("published by the Free Software Foundation, either version 3 of the\n");
	PrintFormat("License, or (at your option) any later version.\n");
	PrintFormat("\n");
	PrintFormat("This program is distributed in the hope that it will be useful,\n");
	PrintFormat("but WITHOUT ANY WARRANTY; without even the implied warranty of\n");
	PrintFormat("MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n");
	PrintFormat("GNU Affero General Public License for more details.\n");
	PrintFormat("\n");
	PrintFormat("You should have received a copy of the GNU Affero General Public License\n");
	PrintFormat("along with this program.  If not, see <https://www.gnu.org/licenses/>.\n");
	PostQuitMessage(0);
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL 
__ParseArgs(
	_In_ 		LPSTATE 	lpsState
){
	LONG lArgCount;
	DLPSTR dlpszArgValues;
	UARCHLONG ualIndex;
	LPSTR lpszKey, lpszValue;

	lArgCount = lpsState->lArgCount;
	dlpszArgValues = lpsState->dlpszArgValues;

	for (
		ualIndex = 0;
		ualIndex < (UARCHLONG)lArgCount;
		ualIndex++
	){
		lpszKey = dlpszArgValues[ualIndex];
	
		if (ualIndex < (UARCHLONG)(lArgCount - 1))
		{ lpszValue = dlpszArgValues[ualIndex + 1] ;}
		else 
		{ lpszValue = NULLPTR; }

		if (
			0 == StringCompare("-h", lpszKey) ||
			0 == StringCompare("--help", lpszKey)
		){ __PrintHelp(); }

		else if (
			0 == StringCompare("-l", lpszKey) ||
			0 == StringCompare("--license", lpszKey)
		){ __PrintLicense(); }

		else if (
			0 == StringCompare("-f", lpszKey) ||
			0 == StringCompare("--file", lpszKey)
		){ 
			if (NULLPTR == lpszValue)
			{ 
				PrintFormat("Please specify a filename!\n");
				PostQuitMessage(1);
			}
			lpsState->lpszFile = lpszValue;
		}

		else if (
			0 == StringCompare("-c", lpszKey) ||
			0 == StringCompare("--create", lpszKey)
		){ lpsState->cmdOperation |= COMMAND_CREATE_DATABASE; }
		
		else if (
			0 == StringCompare("-a", lpszKey) ||
			0 == StringCompare("--add-item", lpszKey)
		){ 
			if (NULLPTR == lpszValue)
			{ 
				PrintFormat("Please specify an item!\n");
				PostQuitMessage(1);
			}
			lpsState->cmdOperation |= COMMAND_INSERT_ITEM;
			lpsState->lpszItem = lpszValue;
		}
		
		else if (
			0 == StringCompare("-A", lpszKey) ||
			0 == StringCompare("--add-inventory", lpszKey)
		){ 
			if (NULLPTR == lpszValue)
			{ 
				PrintFormat("Please specify an inventory listing!\n");
				PostQuitMessage(1);
			}
			lpsState->cmdOperation |= COMMAND_INSERT_INVENTORY;
			lpsState->lpszInventory = lpszValue;
		}

		else if (
			0 == StringCompare("-l", lpszKey) ||
			0 == StringCompare("--list-items", lpszKey)
		){ lpsState->cmdOperation |= COMMAND_LIST_ITEM; }
		
		else if (
			0 == StringCompare("-L", lpszKey) ||
			0 == StringCompare("--list-inventory", lpszKey)
		){ lpsState->cmdOperation |= COMMAND_LIST_INVENTORY; }
		
		else if (
			0 == StringCompare("-I", lpszKey) ||
			0 == StringCompare("--list-inventory-by-id", lpszKey)
		){
			if (NULLPTR == lpszValue)
			{ 
				PrintFormat("Please specify a query!\n");
				PostQuitMessage(1);
			}
			lpsState->cmdOperation |= COMMAND_LIST_INVENTORY_BY_ID; 
			lpsState->lpszInventoryByID = lpszValue;
		}
		
		else if (
			0 == StringCompare("-e", lpszKey) ||
			0 == StringCompare("--edit-item", lpszKey)
		){
			if (NULLPTR == lpszValue)
			{ 
				PrintFormat("Please specify a query!\n");
				PostQuitMessage(1);
			}
			lpsState->cmdOperation |= COMMAND_EDIT_ITEM;
			lpsState->lpszEditItem = lpszValue;
		}
		
		else if (
			0 == StringCompare("-E", lpszKey) ||
			0 == StringCompare("--edit-inventory", lpszKey)
		){ 
			if (NULLPTR == lpszValue)
			{ 
				PrintFormat("Please specify a query!\n");
				PostQuitMessage(1);
			}
			lpsState->cmdOperation |= COMMAND_EDIT_INVENTORY; 
			lpsState->lpszEditInventory = lpszValue;
		}
		
		else if (
			0 == StringCompare("-i", lpszKey) ||
			0 == StringCompare("--increment", lpszKey)
		){ 
			if (NULLPTR == lpszValue)
			{ 
				PrintFormat("Please specify how much to increment by!\n");
				PostQuitMessage(1);
			}
			lpsState->cmdOperation |= COMMAND_INCREMENT_INVENTORY;
			lpsState->lpszIncrement = lpszValue;
		}
		
		else if (
			0 == StringCompare("-d", lpszKey) ||
			0 == StringCompare("--decrement", lpszKey)
		){ 
			if (NULLPTR == lpszValue)
			{ 
				PrintFormat("Please specify how much to decrement by!\n");
				PostQuitMessage(1);
			}
			lpsState->cmdOperation |= COMMAND_DECREMENT_INVENTORY;
			lpsState->lpszDecrement = lpszValue;
		}
		
		else if (
			0 == StringCompare("-s", lpszKey) ||
			0 == StringCompare("--sum-cost", lpszKey)
		){ lpsState->cmdOperation |= COMMAND_PRINT_SUM_TOTALCOST; }
		
		else if (
			0 == StringCompare("-S", lpszKey) ||
			0 == StringCompare("--sum-quantity", lpszKey)
		){ lpsState->cmdOperation |= COMMAND_PRINT_SUM_QUANTITY; }
		
		else if (
			0 == StringCompare("-r", lpszKey) ||
			0 == StringCompare("--run-query", lpszKey)
		){ 
			if (NULLPTR == lpszValue)
			{ 
				PrintFormat("Please specify a query!\n");
				PostQuitMessage(1);
			}
			lpsState->cmdOperation |= COMMAND_RUN_QUERY;
			lpsState->lpszQuery = lpszValue;
		}
	}

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL 
__InsertItem(
	_In_ 		LPSTATE 	lpsState
){
	if (NULLPTR == lpsState->lpszFile)
	{
		PrintFormat("Please specify a file for database creation!\n");
		PostQuitMessage(2);
	}
	
	if (NULL_OBJECT == lpsState->hInventory)
	{
		lpsState->hInventory = OpenInventoryDatabase(lpsState->lpszFile);
		if (NULL_OBJECT == lpsState->hInventory)
		{
			PrintFormat("Could not open %s\n", lpsState->lpszFile);
			PostQuitMessage(2);
		}
	}

	InventoryAddNewItem(lpsState->hInventory, lpsState->lpszItem);
	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL 
__InsertInventory(
	_In_ 		LPSTATE 	lpsState
){
	DLPSTR dlpszSplit;
	UARCHLONG ualCount;
	ULONG ulItemID, ulQuantity;
	LPSTR lpszAcquiredDate, lpszExpirationDate;
	FLOAT fCostPerUnit, fTotalCost;

	if (NULLPTR == lpsState->lpszFile)
	{
		PrintFormat("Please specify a file for database creation!\n");
		PostQuitMessage(2);
	}
	
	if (NULL_OBJECT == lpsState->hInventory)
	{
		lpsState->hInventory = OpenInventoryDatabase(lpsState->lpszFile);
		if (NULL_OBJECT == lpsState->hInventory)
		{
			PrintFormat("Could not open %s\n", lpsState->lpszFile);
			PostQuitMessage(2);
		}
	}

	ualCount = StringSplit(lpsState->lpszInventory, ";", &dlpszSplit);
	PrintFormat("%lu\n", ualCount);

	if (6 != ualCount && 7 != ualCount)
	{
		PrintFormat("Please use the following format for adding a new item!\n");
		PrintFormat("ItemID;Quantity;AcquiredDate;ExpirationDate;CostPerUnit;TotalCost;Notes[511]\n");
		PostQuitMessage(2);
	}

	StringScanFormat(dlpszSplit[0], "%u", &ulItemID);
	StringScanFormat(dlpszSplit[1], "%u", &ulQuantity);
	lpszAcquiredDate = dlpszSplit[2];
	lpszExpirationDate = dlpszSplit[3];
	StringScanFormat(dlpszSplit[4], "%f", &fCostPerUnit);
	StringScanFormat(dlpszSplit[5], "%f", &fTotalCost);
	
	if (6 == ualCount)
	{
		InventoryAddNewInventory(
			lpsState->hInventory,
			ulItemID,
			ulQuantity,
			(LPCSTR)lpszAcquiredDate,
			(LPCSTR)lpszExpirationDate,
			fCostPerUnit,
			fTotalCost,
			NULLPTR
		);
	}
	else 
	{
		InventoryAddNewInventory(
			lpsState->hInventory,
			ulItemID,
			ulQuantity,
			(LPCSTR)lpszAcquiredDate,
			(LPCSTR)lpszExpirationDate,
			fCostPerUnit,
			fTotalCost,
			(LPCSTR)dlpszSplit[6]
		);
	}

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL 
__EditItem(
	_In_ 		LPSTATE 	lpsState
){
	UARCHLONG ualCount;
	DLPSTR dlpszSplit;
	ULONG ulID;

	if (NULLPTR == lpsState->lpszFile)
	{
		PrintFormat("Please specify a file for database creation!\n");
		PostQuitMessage(2);
	}
	
	if (NULL_OBJECT == lpsState->hInventory)
	{
		lpsState->hInventory = OpenInventoryDatabase(lpsState->lpszFile);
		if (NULL_OBJECT == lpsState->hInventory)
		{
			PrintFormat("Could not open %s\n", lpsState->lpszFile);
			PostQuitMessage(2);
		}
	}

	ualCount = StringSplit(lpsState->lpszEditItem, ";", &dlpszSplit);
	if (2 != ualCount)
	{
		PrintFormat("Please use the following format for item editting!\n");
		PrintFormat("ID;Name\n");
		PostQuitMessage(2);
	}

	StringScanFormat(dlpszSplit[0], "%u", &ulID);
	InventoryEditItem(lpsState->hInventory, ulID, dlpszSplit[1]);

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL 
__EditInventory(
	_In_ 		LPSTATE 	lpsState
){
	DLPSTR dlpszSplit;
	UARCHLONG ualCount;
	ULONG ulID, ulItemID, ulQuantity;
	LPSTR lpszAcquiredDate, lpszExpirationDate;
	FLOAT fCostPerUnit, fTotalCost;

	if (NULLPTR == lpsState->lpszFile)
	{
		PrintFormat("Please specify a file for database creation!\n");
		PostQuitMessage(2);
	}
	
	if (NULL_OBJECT == lpsState->hInventory)
	{
		lpsState->hInventory = OpenInventoryDatabase(lpsState->lpszFile);
		if (NULL_OBJECT == lpsState->hInventory)
		{
			PrintFormat("Could not open %s\n", lpsState->lpszFile);
			PostQuitMessage(2);
		}
	}

	ualCount = StringSplit(lpsState->lpszEditInventory, ";", &dlpszSplit);

	if (7 != ualCount && 8 != ualCount)
	{
		PrintFormat("Please use the following format for adding a new item!\n");
		PrintFormat("ID;ItemID;Quantity;AcquiredDate;ExpirationDate;CostPerUnit;TotalCost;Notes[511]\n");
		PostQuitMessage(2);
	}

	StringScanFormat(dlpszSplit[0], "%u", &ulID);
	StringScanFormat(dlpszSplit[1], "%u", &ulItemID);
	StringScanFormat(dlpszSplit[2], "%u", &ulQuantity);
	lpszAcquiredDate = dlpszSplit[3];
	lpszExpirationDate = dlpszSplit[4];
	StringScanFormat(dlpszSplit[5], "%f", &fCostPerUnit);
	StringScanFormat(dlpszSplit[6], "%f", &fTotalCost);
	
	if (7 == ualCount)
	{
		InventoryEditInventory(
			lpsState->hInventory,
			ulID,
			ulItemID,
			ulQuantity,
			(LPCSTR)lpszAcquiredDate,
			(LPCSTR)lpszExpirationDate,
			fCostPerUnit,
			fTotalCost,
			NULLPTR
		);
	}
	else 
	{
		InventoryEditInventory(
			lpsState->hInventory,
			ulID,
			ulItemID,
			ulQuantity,
			(LPCSTR)lpszAcquiredDate,
			(LPCSTR)lpszExpirationDate,
			fCostPerUnit,
			fTotalCost,
			(LPCSTR)dlpszSplit[7]
		);
	}

	return TRUE;
}




_Success_(return == 0, _Non_Locking_)
INTERNAL_OPERATION
LONG
__RunQuery(
	_In_ 		LPVOID 			lpParam,
	_In_ 		LONG 			lColCount,
	_In_ 		DLPSTR 			dlpszValues,
	_In_ 		DLPSTR 			dlpszColumns
){
	LPBOOL lpbHasPrintedNames;
	UARCHLONG ualIndex;

	lpbHasPrintedNames = lpParam;

	if (FALSE == *lpbHasPrintedNames)
	{
		for (
			ualIndex = 0;
			ualIndex < (UARCHLONG)lColCount;
			ualIndex++
		){
			if (ualIndex != (UARCHLONG)lColCount - 1)
			{ PrintFormat("%s|",dlpszColumns[ualIndex]); }
			else 
			{ PrintFormat("%s", dlpszColumns[ualIndex]); }
		}
		*lpbHasPrintedNames = TRUE;
		PrintFormat("\n");
	}

	for (
		ualIndex = 0;
		ualIndex < (UARCHLONG)lColCount;
		ualIndex++
	){
		if (ualIndex != (UARCHLONG)lColCount - 1)
		{ PrintFormat("%s|",dlpszValues[ualIndex]); }
		else 
		{ PrintFormat("%s", dlpszValues[ualIndex]); }
	}
	PrintFormat("\n");

	return 0;
}




_Success_(return == 0, _Non_Locking_)
LONG 
Main(
	_In_ 		LONG 		lArgCount,
	_In_Opt_	DLPSTR 		dlpszArgValues
){
	STATE sProgram;
	ZeroMemory(&sProgram, sizeof(STATE));

	sProgram.lArgCount = lArgCount;
	sProgram.dlpszArgValues = dlpszArgValues;

	__ParseArgs(&sProgram);
	
	if (sProgram.cmdOperation & COMMAND_CREATE_DATABASE)
	{
		if (NULLPTR == sProgram.lpszFile)
		{
			PrintFormat("Please specify a file for database creation!\n");
			PostQuitMessage(1);
		}
		CreateInventoryDatabase(sProgram.lpszFile);
	}

	if (sProgram.cmdOperation & COMMAND_INSERT_ITEM)
	{ __InsertItem(&sProgram); }

	if (sProgram.cmdOperation & COMMAND_INSERT_INVENTORY)
	{ __InsertInventory(&sProgram); }

	if (sProgram.cmdOperation & COMMAND_LIST_ITEM)
	{ 
		if (NULLPTR == sProgram.lpszFile)
		{
			PrintFormat("Please specify a file for database creation!\n");
			PostQuitMessage(2);
		}

		if (NULL_OBJECT == sProgram.hInventory)
		{
			sProgram.hInventory = OpenInventoryDatabase(sProgram.lpszFile);
			if (NULL_OBJECT == sProgram.hInventory)
			{
				PrintFormat("Could not open %s\n", sProgram.lpszFile);
				PostQuitMessage(2);
			}
		}
		InventoryListItems(sProgram.hInventory); 
	}

	if (sProgram.cmdOperation & COMMAND_LIST_INVENTORY)
	{ 
		if (NULLPTR == sProgram.lpszFile)
		{
			PrintFormat("Please specify a file for database creation!\n");
			PostQuitMessage(2);
		}

		if (NULL_OBJECT == sProgram.hInventory)
		{
			sProgram.hInventory = OpenInventoryDatabase(sProgram.lpszFile);
			if (NULL_OBJECT == sProgram.hInventory)
			{
				PrintFormat("Could not open %s\n", sProgram.lpszFile);
				PostQuitMessage(2);
			}
		}
		InventoryListInventory(sProgram.hInventory); 
	}
	
	if (sProgram.cmdOperation & COMMAND_LIST_INVENTORY_BY_ID)
	{ 
		ULONG ulItemID;
		if (NULLPTR == sProgram.lpszFile)
		{
			PrintFormat("Please specify a file for database creation!\n");
			PostQuitMessage(2);
		}

		if (NULL_OBJECT == sProgram.hInventory)
		{
			sProgram.hInventory = OpenInventoryDatabase(sProgram.lpszFile);
			if (NULL_OBJECT == sProgram.hInventory)
			{
				PrintFormat("Could not open %s\n", sProgram.lpszFile);
				PostQuitMessage(2);
			}
		}

		StringScanFormat(sProgram.lpszInventoryByID, "%u", &ulItemID);
		InventoryListInventoryById(sProgram.hInventory, ulItemID);
	}

	if (sProgram.cmdOperation & COMMAND_EDIT_ITEM)
	{ __EditItem(&sProgram); }

	if (sProgram.cmdOperation & COMMAND_EDIT_INVENTORY)
	{ __EditInventory(&sProgram); }

	if (sProgram.cmdOperation & COMMAND_INCREMENT_INVENTORY)
	{
		ULONG ulItemID;
		ULONG ulDelta;
		UARCHLONG ualCount;
		DLPSTR dlpszSplit;

		if (NULLPTR == sProgram.lpszFile)
		{
			PrintFormat("Please specify a file for database creation!\n");
			PostQuitMessage(2);
		}

		if (NULL_OBJECT == sProgram.hInventory)
		{
			sProgram.hInventory = OpenInventoryDatabase(sProgram.lpszFile);
			if (NULL_OBJECT == sProgram.hInventory)
			{
				PrintFormat("Could not open %s\n", sProgram.lpszFile);
				PostQuitMessage(2);
			}
		}

		ualCount = StringSplit(sProgram.lpszIncrement, ";", &dlpszSplit);

		if (2 != ualCount)
		{
			PrintFormat("Please specify the ItemID and Quantity!\n");
			PrintFormat("ItemID;Quantity");
			PostQuitMessage(2);
		}

		StringScanFormat(dlpszSplit[0], "%u", &ulItemID);
		StringScanFormat(dlpszSplit[1], "%u", &ulDelta);
		DestroySplitString(dlpszSplit);

		InventoryIncrementQuantityById(sProgram.hInventory, ulItemID, ulDelta);
	}

	if (sProgram.cmdOperation & COMMAND_DECREMENT_INVENTORY)
	{
		ULONG ulItemID;
		ULONG ulDelta;
		UARCHLONG ualCount;
		DLPSTR dlpszSplit;

		if (NULLPTR == sProgram.lpszFile)
		{
			PrintFormat("Please specify a file for database creation!\n");
			PostQuitMessage(2);
		}

		if (NULL_OBJECT == sProgram.hInventory)
		{
			sProgram.hInventory = OpenInventoryDatabase(sProgram.lpszFile);
			if (NULL_OBJECT == sProgram.hInventory)
			{
				PrintFormat("Could not open %s\n", sProgram.lpszFile);
				PostQuitMessage(2);
			}
		}

		ualCount = StringSplit(sProgram.lpszDecrement, ";", &dlpszSplit);

		if (2 != ualCount)
		{
			PrintFormat("Please specify the ItemID and Quantity!\n");
			PrintFormat("ItemID;Quantity");
			PostQuitMessage(2);
		}

		StringScanFormat(dlpszSplit[0], "%u", &ulItemID);
		StringScanFormat(dlpszSplit[1], "%u", &ulDelta);
		DestroySplitString(dlpszSplit);

		InventoryDecrementQuantityById(sProgram.hInventory, ulItemID, ulDelta);
	}

	if (sProgram.cmdOperation & COMMAND_PRINT_SUM_TOTALCOST)
	{
		if (NULL_OBJECT == sProgram.hInventory)
		{
			sProgram.hInventory = OpenInventoryDatabase(sProgram.lpszFile);
			if (NULL_OBJECT == sProgram.hInventory)
			{
				PrintFormat("Could not open %s\n", sProgram.lpszFile);
				PostQuitMessage(2);
			}
		}

		InventorySumTotalCost(sProgram.hInventory);
	}
	
	if (sProgram.cmdOperation & COMMAND_PRINT_SUM_QUANTITY)
	{
		if (NULL_OBJECT == sProgram.hInventory)
		{
			sProgram.hInventory = OpenInventoryDatabase(sProgram.lpszFile);
			if (NULL_OBJECT == sProgram.hInventory)
			{
				PrintFormat("Could not open %s\n", sProgram.lpszFile);
				PostQuitMessage(2);
			}
		}

		InventorySumQuantity(sProgram.hInventory);
	}

	if (sProgram.cmdOperation & COMMAND_RUN_QUERY)
	{
		BOOL bPrintedColumn;
		bPrintedColumn = FALSE;

		if (NULLPTR == sProgram.lpszFile)
		{
			PrintFormat("Please specify a file for database creation!\n");
			PostQuitMessage(2);
		}

		if (NULL_OBJECT == sProgram.hInventory)
		{
			sProgram.hInventory = OpenInventoryDatabase(sProgram.lpszFile);
			if (NULL_OBJECT == sProgram.hInventory)
			{
				PrintFormat("Could not open %s\n", sProgram.lpszFile);
				PostQuitMessage(2);
			}
		}
		RunInventoryQuery(
			sProgram.hInventory,
			sProgram.lpszQuery,
			__RunQuery,
			&bPrintedColumn
		);
	}

	

	if (NULL_OBJECT != sProgram.hInventory)
	{ DestroyObject(sProgram.hInventory); }

	return 0;
}