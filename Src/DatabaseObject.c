/*

   ____ ___                      _                   
 / ___|_ _|_ ____   _____ _ __ | |_ ___  _ __ _   _ 
| |    | || '_ \ \ / / _ \ '_ \| __/ _ \| '__| | | |
| |___ | || | | \ V /  __/ | | | || (_) | |  | |_| |
 \____|___|_| |_|\_/ \___|_| |_|\__\___/|_|   \__, |
                                              |___/ 

A C Program For Keeping Track Of An Inventory.
Copyright (C) 2019 Zach Podbielniak

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


#ifndef CINVENTORY_DATABASEOBJECT_C
#define CINVENTORY_DATABASEOBJECT_C


#include "Prereqs.h"


typedef struct __INVENTORY_DATABASE
{
	INHERITS_FROM_HANDLE();
	HANDLE 		hLock;
	sqlite3 	*slDb;
	LPSTR 		lpszError;
} INVENTORY_DATABASE, *LPINVENTORY_DATABASE, **DLPINVENTORY_DATABASE;


#endif