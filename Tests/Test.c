#include "../Src/Create.h"
#include "../Src/Database.h"


LONG
__Query(
	_In_Opt_	LPVOID 		lpParam,
	_In_ 		LONG 		lArgCount,
	_In_Z_ 		DLPSTR 		dlpszValues,
	_In_Z_ 		DLPSTR 		dlpszColumns
){
	UNREFERENCED_PARAMETER(lpParam);

	UARCHLONG ualIndex;
	LPBOOL lpbIsFirst;
	lpbIsFirst = lpParam;

	if (FALSE == *lpbIsFirst)
	{
		for (
			ualIndex = 0;
			ualIndex < (UARCHLONG)lArgCount;
			ualIndex++
		){
			PrintFormat("%s\t", dlpszColumns[ualIndex]);
		}
		PrintFormat("\n-------------------------------------------\n");
		*lpbIsFirst = TRUE;
	}

	for (
		ualIndex = 0;
		ualIndex < (UARCHLONG)lArgCount;
		ualIndex++
	){
		PrintFormat("%s\t", dlpszValues[ualIndex]);
	}
	PrintFormat("\n");

	return 0;
}




LONG Main()
{
	HANDLE hInventory;
	BOOL bIsFirst;
	
	CreateInventoryDatabase("bob.db");
	hInventory = OpenInventoryDatabase("bob.db");

	InventoryAddNewItem(
		hInventory,
		".308 Winchester"
	);

	InventoryAddNewItem(
		hInventory,
		".223 Remington"
	);

	InventoryAddNewItem(
		hInventory,
		"5.56x45mm NATO"
	);
	
	bIsFirst = FALSE;
	RunInventoryQuery(
		hInventory,
		"SELECT * FROM Items;",
		__Query,
		&bIsFirst
	);


	return 0;
}