#    ____ ___                      _                   
#  / ___|_ _|_ ____   _____ _ __ | |_ ___  _ __ _   _ 
# | |    | || '_ \ \ / / _ \ '_ \| __/ _ \| '__| | | |
# | |___ | || | | \ V /  __/ | | | || (_) | |  | |_| |
#  \____|___|_| |_|\_/ \___|_| |_|\__\___/|_|   \__, |
#                                               |___/

# A C Program For Keeping Track Of An Inventory.
# Copyright (C) 2019 Zach Podbielniak

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


CC = gcc
ASM = nasm
STD = -std=c89
WARNINGS = -Wall -Wextra -Wshadow -Wunsafe-loop-optimizations -Wpointer-arith
WARNINGS += -Wfloat-equal -Wswitch-enum -Wstrict-aliasing -Wno-missing-braces
WARNINGS += -Wno-cast-function-type -Wno-stringop-truncation #-Wno-switch-enum
DEFINES = -D _DEFAULT_SOURCE
DEFINES += -D _GNU_SOURCE

DEFINES_D = $(DEFINES)
DEFINES_D += -D __DEBUG__

OPTIMIZE = -O2 -funroll-loops -fstrict-aliasing
OPTIMIZE += -fstack-protector-strong
#MARCH = -march=native
#MTUNE = -mtune=native

CC_FLAGS = $(STD)
CC_FLAGS += $(WARNINGS)
CC_FLAGS += $(DEFINES)
CC_FLAGS += $(OPTIMIZE)
#CC_FLAGS += $(MARCH)
#CC_FLAGS += $(MTUNE)

CC_FLAGS_D = $(STD)
CC_FLAGS_D += $(WARNINGS)
CC_FLAGS_D += $(DEFINES_D)
#CC_FLAGS_D += -fsanitize=address

CC_FLAGS_T = $(CC_FLAGS_D)
CC_FLAGS_T += -Wno-unused-variable

ASM_FLAGS = -Wall



FILES = Create.o Database.o
FILES_D = Create_d.o Database_d.o




all:	bin libcinventory.so 
debug:	bin libcinventory_d.so 
test: 	bin $(TEST)

bin:
	mkdir -p bin/

clean: 	bin
	rm -rf bin/

check:	bin 
	cppcheck . --std=c89 -j $(shell nproc) --inline-suppr --error-exitcode=1



# Install

install:
	cp bin/libcinventory.so /usr/lib/

install_debug:
	cp bin/libcinventory_d.so /usr/lib/

install_headers:
	rm -rf /usr/include/CInventory/
	mkdir -p /usr/include/CInventory
	find . -type f -name "*.h" -exec install -D {} /usr/include/CInventory/{} \;
	mv /usr/include/CInventory/Src/* /usr/include/CInventory
	rm -rf /usr/include/CInventory/Src



libcinventory.so: $(FILES)
	$(CC) -shared -fPIC -s -o bin/libcinventory.so bin/*.o -pthread -lpodnet -lsqlite3 $(STD) $(OPTIMIZE) $(MARCH)
	rm bin/*.o

libcinventory_d.so: $(FILES_D)
	$(CC) -g -shared -fPIC -o bin/libcinventory_d.so bin/*_d.o -pthread -lpodnet_d -lsqlite3 $(STD)
	rm bin/*.o

app: bin
	$(CC) -fPIC -o bin/inventory Src/Main.c -lpodnet -lcinventory $(CC_FLAGS) $(STD) $(OPTIMIZE)
	$(CC) -fPIC -o bin/inventoryweb Src/WebAccess.c -lpodnet -lcinventory -lcrock $(CC_FLAGS) $(STD) $(OPTIMIZE)

app_debug: bin
	$(CC) -g -fPIC -o bin/inventory_d Src/Main.c -lpodnet_d -lcinventory_d $(CC_FLAGS_D)
	$(CC) -g -fPIC -o bin/inventoryweb_d Src/WebAccess.c -lpodnet_d -lcinventory_d -lcrock_d $(CC_FLAGS_D)

test:
	$(CC) -g -fPIC -o bin/test Tests/Test.c -lpodnet_d -lcinventory_d $(CC_FLAGS_T)

test_prod:
	$(CC) -fPIC -o bin/test Tests/Test.c -lpodnet -lcinventory $(CC_FLAGS) $(STD) $(OPTIMIZE) $(MARCH)




Create.o: 
	$(CC) -fPIC -c -o bin/Create.o Src/Create.c $(CC_FLAGS)

Database.o:
	$(CC) -fPIC -c -o bin/Database.o Src/Database.c $(CC_FLAGS)






Create_d.o: 
	$(CC) -g -fPIC -c -o bin/Create_d.o Src/Create.c $(CC_FLAGS_D)

Database_d.o:
	$(CC) -g -fPIC -c -o bin/Database_d.o Src/Database.c $(CC_FLAGS_D)